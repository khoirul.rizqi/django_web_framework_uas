from .utils import Render
from .models import Penduduk
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView, View

# Create your views here.
var = {
	'judul' : 'Sistem Informasi Penduduk',
	'info'	: 'untuk tugas Ujian Akhir Semester Pemrograman Jaringan',
	'oleh'	: '1931733121 1931733137',
}

def index(self):
	var['penduduk']=Penduduk.objects.values('id','nik_penduduk','nama_penduduk').order_by('nik_penduduk')
	return render(self,'penduduk/index.html',context=var)

class DetailData(DetailView):
	model = Penduduk
	template_name = 'penduduk/detail.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class TambahData(CreateView):
	model = Penduduk
	fields = '__all__'
	template_name = 'penduduk/insert.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class EditData(UpdateView):
	model = Penduduk
	fields = ['nik_penduduk','nama_penduduk','jenis_kelamin','tanggal_lahir','agama','pendidikan','jenis_pekerjaan']
	template_name = 'penduduk/edit.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class DeleteData(DeleteView):
	model = Penduduk
	template_name = 'penduduk/delete.html'
	success_url = reverse_lazy('home_page')

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class DataToPdf(View):
	def get(self,request):
		var = {
			'penduduk' : Penduduk.objects.all(),
			'request' : request
		}
		return Render.to_pdf(self,'penduduk/print_pdf.html',var)