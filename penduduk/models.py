from django.db import models
from django.urls import reverse
# Create your models here.
class Penduduk(models.Model):
	JK_KELAMIN = (
		('lk','LAKI-LAKI'),
		('pr','PEREMPUAN'),
	)

	nik_penduduk = models.CharField("NIK",max_length=255)
	nama_penduduk = models.CharField("Nama Penduduk",max_length=255)
	jenis_kelamin = models.CharField("Jenis Kelamin",max_length=2, choices=JK_KELAMIN)
	tanggal_lahir = models.DateField("Tanggal Lahir")
	agama = models.CharField("Agama",max_length=255)
	pendidikan = models.CharField("Pendidikan",max_length=255)
	jenis_pekerjaan = models.CharField("Jenis Pekerjaan",max_length=255)

	class Meta:
		ordering = ['nik_penduduk']

	def __str__(self):
		return self.nik_penduduk

	def get_absolute_url(self):
		return reverse('home_page')