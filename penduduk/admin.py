from django.contrib import admin
from .models import Penduduk

# Register your models here.
@admin.register(Penduduk)
class PendudukAdmin(admin.ModelAdmin):
	list_display = [
	'nik_penduduk',
	'nama_penduduk',
	]
	list_filter = [
	'nik_penduduk',
	'nama_penduduk',
	'tanggal_lahir',
	]
	search_fields = [
	'nik_penduduk',
	'nama_penduduk',
	'agama',
	'penduduk',
	'jenis_pekerjaan',
	]