"""UAS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import index, DetailData, TambahData, EditData, DeleteData, DataToPdf

urlpatterns = [
    path('',index,name='home_page'),
    path('penduduk/<int:pk>', DetailData.as_view(),name='detail_data'),
    path('penduduk/add',TambahData.as_view(),name='insert_data'),
    path('penduduk/edit/<int:pk>',EditData.as_view(),name='edit_data'),
    path('penduduk/delete/<int:pk>',DeleteData.as_view(),name='delete_data'),
    path('penduduk/print_pdf',DataToPdf.as_view(),name='data_pdf')
]
